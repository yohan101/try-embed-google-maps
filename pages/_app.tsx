import "../styles/globals.css";

import type { AppProps } from "next/app";
import { Wrapper } from "@googlemaps/react-wrapper";

function MyApp({ Component, pageProps }: AppProps) {
  const API_KEY = process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY || "";

  return (
    <Wrapper apiKey={API_KEY}>
      <Component {...pageProps} />
    </Wrapper>
  );
}

export default MyApp;
