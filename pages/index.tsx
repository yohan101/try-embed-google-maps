import { useRef } from "react";
import { GoogleMap } from "@react-google-maps/api";

import styles from "../styles/Home.module.css";
import type { NextPage } from "next";

const Home: NextPage = () => {
  const mapOptions = {
    fullscreenControl: false,
  };

  const googleMapRef = useRef<GoogleMap>(null);

  return (
    <div className={styles.container}>
      <GoogleMap
        ref={googleMapRef}
        options={mapOptions}
        zoom={10}
        center={{
          lat: 33,
          lng: -83,
        }}
      />
    </div>
  );
};

export default Home;
